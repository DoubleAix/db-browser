## 2019-11-01 updated by Aix

## Set DB Browser service Environment
```bash
cd [projects_root_folder] ## /ods/odsai/projects
git clone https://DoubleAix@bitbucket.org/DoubleAix/db-browser.git
cd db-browser
## odsdmai(for on line)
/ods/odsai/anaconda/bin/virtualenv --extra-search-dir=/ods/odsai/resources/python_packages --never-download venv
source venv/bin/activate
pip install -r requirements.txt --no-index --find-links file:/ods/odsai/resources/python_packages/
deactivate

cd src/app/statics
npm install
```
## Start DB Browser service
```bash
cd db-browser
source venv/bin/activate

cd src
flask run
## if you want to set other parameters such like service location or port, you can perform following command to meet your project's need.
## FLASK_ENV=development flask run -h XX.XX.XX.XX -p XXXX

## if you would like to use gunicorn, performing following commands
flask gunicorn run dev
flask gunicorn run prod ## prod
flask gunicorn clean ## clean gunicorn realated files

## open a new terminal
flask ng build dev
## If you have finished your development stage, you have to compile your Angular project to the production edition.
flask ng build prod
```

## Use DB Browser service
- Point your browser at http://localhost:5000/dev/#/query
