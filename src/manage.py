import os
import click
import subprocess
from pathlib import Path

# from flask_migrate import Migrate
from flask.cli import AppGroup
from app.main import create_app
# from app.main import create_app, db
# from app.main.utils.init_data import init_config

with open('/ods/odsai/CURRENT_ENV', 'r') as myfile:
    current_env = myfile.read().replace('\n', '')
# current_env = 'TEST'
app = create_app(current_env)

# migrate = Migrate(app, db)

## Flask CLI
init_cli = AppGroup('init', help='Init data.')

@init_cli.command('db', help='Reset DB.')
def reset_db():
    subprocess.run(["rm", "web_config.db"])
    subprocess.run(["rm", "-r", "migrations"])
    subprocess.run(["flask", "db", "init"])
    subprocess.run(["flask", "db", "migrate", "--message", "initial database migration"])
    subprocess.run(["flask", "db", "upgrade"])

# @init_cli.command('config', help='Init config.')
# def initdata():
#     init_config(app)


app.cli.add_command(init_cli)

## gunicorn CLI
gunicorn_cli = AppGroup('gunicorn', help='Gunicorn command.')
root_dir = Path(os.path.dirname(__file__)).parent
logs_folder_path = "{}/logs".format(root_dir)

@gunicorn_cli.command('run', help='Run gunicorn')
@click.argument('env')
def run_gunicorn(env):
    if not os.path.exists(logs_folder_path):
        os.makedirs(logs_folder_path)
    if env == 'prod':
        subprocess.run([
            "gunicorn",
            "--workers", "8", 
            "--bind", "127.0.0.1:11111",
            "--limit-request-field_size","131072" , 
            "--access-logfile", "{}/access.log".format(logs_folder_path) ,
            "--error-logfile" ,"{}/error.log".format(logs_folder_path), 
            "--pid", "{}/gunicorn.pid".format(logs_folder_path), 
            "wsgi:app"
            ])
    if env == 'dev':
        subprocess.run([
            "gunicorn", 
            "--workers", "8", 
            "--bind", "127.0.0.1:11111",
            "--limit-request-field_size","131072" , 
            "--access-logfile", "{}/access.log".format(logs_folder_path) ,
            "--error-logfile" ,"{}/error.log".format(logs_folder_path), 
            "--pid", "{}/gunicorn.pid".format(logs_folder_path), 
            # "--reload", "True",
            "wsgi:app"
            ])

@gunicorn_cli.command('clean', help='Clean gunicorn related files')
def clean_gunicorn():
    subprocess.run(["rm","-r",logs_folder_path])


app.cli.add_command(gunicorn_cli)

# ng CLI
ng_cli = AppGroup('ng', help='NG build command.')

base_dir = Path(os.path.dirname(__file__))
angular_project_path = "{}/app/static".format(base_dir)


@ng_cli.command('build', help='Build Angular project.')
@click.argument('env')
def build(env):
    if env == 'prod':
        subprocess.run([
            "ng", "build", "--prod", "--build-optimizer",
            "--output-path=dist/prod"
        ],
                       cwd=angular_project_path)
    if env == 'dev':
        subprocess.run([
            "ng", "build", "--watch", "--output-path=dist/dev",
            "--base-href=/dev/"
        ],
                       cwd=angular_project_path)


app.cli.add_command(ng_cli)