from flask_restplus import Namespace, fields


class CxOracleDto:
    api = Namespace('cx_oracle', description='CX Oracle related operations')
    request_body = api.model('Query Request Body', {
        'sql': fields.String(required=True, description='SQL statement', example='SELECT * FROM POST_ADDR'),
        # 'password': fields.String(required=True, description='password', example='00000000')
    })
