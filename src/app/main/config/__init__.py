import os
from pathlib import Path
from fubon_utils.encryption.get_connection_info_g import get_connection_info_g

BASE_DIR = Path(os.path.dirname(__file__)).parents[2]
STATIC_BASE_DIR = Path(os.path.dirname(__file__)).parents[1]

if os.name == 'nt':
    CONFIG_FILE_PATH = 'D:\ods\odsai\config.ini'
else:  ## os.name =='posix'
    CONFIG_FILE_PATH = '/ods/odsai/config.ini'

with open('/ods/odsai/CURRENT_ENV', 'r') as myenv:
        ENV = myenv.read().replace('\n', '')

class Config:
    ## cx_Oracle
    CX_ORACLE_ACCOUNT = get_connection_info_g(session=ENV, key='oracle_account' , path=CONFIG_FILE_PATH, encrypt=False)
    CX_ORACLE_PASSWORD = get_connection_info_g(session=ENV, key='oracle_password' , path=CONFIG_FILE_PATH, encrypt=True)
    CX_ORACLE_LOCATION = get_connection_info_g(session=ENV, key='oracle_location' , path=CONFIG_FILE_PATH, encrypt=False)
    ## dashboard blueprint
    STATIC_FOLDER = os.path.join(STATIC_BASE_DIR, 'static/dist/prod')
    STATIC_FOLDER_DEV = os.path.join(STATIC_BASE_DIR, 'static/dist/dev')
    UPLOAD_FOLDER = os.path.join(STATIC_BASE_DIR, 'static/tmp/upload_file')


class DevelopmentConfig(Config):
    pass

class TestingConfig(Config):
    pass

class UserTestingConfig(Config):
    pass

class ProductionConfig(Config):
    pass


config_by_name = dict(DEFAULT=Config,
                      DEV=DevelopmentConfig,
                      TEST=TestingConfig,
                      UAT=UserTestingConfig,
                      PROD=ProductionConfig)