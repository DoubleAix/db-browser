import cx_Oracle
from flask import current_app, _app_ctx_stack


class CxOracle(object):
    def __init__(self, app=None):
        self.app = app
        if app is not None:
            self.init_app(app)

    def init_app(self, app):
        app.teardown_appcontext(self.teardown)

    def connect(self):
        return cx_Oracle.connect(
            current_app.config['CX_ORACLE_ACCOUNT'], 
            current_app.config['CX_ORACLE_PASSWORD'], 
            current_app.config['CX_ORACLE_LOCATION'], 
            encoding="UTF-8"
            )
    
    def teardown(self, exception):
        ctx = _app_ctx_stack.top
        if hasattr(ctx, 'cx_oracle_db'):
            ctx.cx_oracle_db.close()

    @property
    def connection(self):
        ctx = _app_ctx_stack.top
        if ctx is not None:
            if not hasattr(ctx, 'cx_oracle_db'):
                ctx.cx_oracle_db = self.connect()
            return ctx.cx_oracle_db