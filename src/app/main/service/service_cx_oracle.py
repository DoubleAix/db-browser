import pandas as pd
import numpy as np
from app.main import oracle

def query_cx_oracle(data):
    try:
        cursor = oracle.connection.cursor()
        cursor.execute(data['sql'])
        columns = [i[0] for i in cursor.description]
        df = pd.DataFrame(data=cursor.fetchall(), columns=columns)
        # .replace({np.nan:None})
        df_dict = df.where((pd.notnull(df)), None).to_dict(orient='records')
        return {'data':df_dict, 'columns': columns}, 200
    except Exception as e:
        return {'message': e.__str__()}, 400