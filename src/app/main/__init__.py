from flask import Flask
# from flask_sqlalchemy import SQLAlchemy
# from flask_jwt_extended import JWTManager

import datetime

from app.main.config import config_by_name

# from flask_end2end.end2end import End2End
from app.main.utils.extension_cx_oracle import CxOracle

oracle = CxOracle()
# db = SQLAlchemy()
# jwt_manager = JWTManager()
# e2e = End2End()


def create_app(config_name="DEV"):
    app = Flask(__name__)

    ## end2end config
    # app.config['SYSTEM_NAME'] = 'explore_platform'        
    # app.config['PATH'] = './'                    
    # app.config['AUDIT_PATH'] = './'               
    # app.config['FIRST'] = True

    ## load config into flask instance
    app.config.from_object(config_by_name[config_name])

    ## register blueprint
    from app.main.blueprint.blueprint_dashboard import dashboard
    from app.main.blueprint.blueprint_api import blueprint_api
    from app.main.blueprint.blueprint_dashboard_dev import dashboard_dev
    # from app.main.blueprint.blueprint_test import test
    
    app.register_blueprint(blueprint_api, url_prefix='/api')
    app.register_blueprint(dashboard, url_prefix='/')
    if config_name == "DEV" or config_name == "TEST":
        app.register_blueprint(dashboard_dev, url_prefix='/dev')

    # app.register_blueprint(test, url_prefix='/test')
    
    ## init extension
    oracle.init_app(app)
    # db.init_app(app)
    # jwt_manager.init_app(app)
    # e2e.init_app(app)

        
    return app