from flask import request, jsonify
from flask_restplus import Resource

from app.main.service.service_cx_oracle import query_cx_oracle
from app.main.dto.dto_cx_oracle import CxOracleDto

api = CxOracleDto.api
_request_body = CxOracleDto.request_body

@api.route('/')
class Query(Resource):
    @api.response(200, 'Query Success')
    @api.response(400, 'Query Failure')
    @api.expect(_request_body)
    # @api.marshal_with(_response_body)
    def post(self):

        data = request.json
        return query_cx_oracle(data)
       