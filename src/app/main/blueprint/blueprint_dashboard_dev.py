from flask import Blueprint
from app.main.config import Config
# from flask import current_app
# from app import blueprint

dashboard_dev = Blueprint('dashboard_dev',
                      __name__,
                      static_folder=Config.STATIC_FOLDER_DEV,
                      static_url_path='')


@dashboard_dev.after_request
def add_header(response):
    response.headers['X-UA-Compatible'] = 'IE=Edge,chrome=1'
    response.headers['Cache-Control'] = 'public, max-age=0'
    return response


@dashboard_dev.route('/')
def home():
    return dashboard_dev.send_static_file('index.html')