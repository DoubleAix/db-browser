from flask_restplus import Api
from flask import Blueprint

# from app.main import jwt_manager
from app.main.controller.controller_cx_oracle import api as cx_oracle_ns
# from app.main.controller.controller_config import api as config_ns
# from app.main.controller.controller_user import api as user_ns
# from app.main.controller.controller_group import api as group_ns
# from app.main.controller.controller_permission import api as permission_ns
# from app.main.controller.controller_parent_resource import api as parent_resource_ns
# from app.main.controller.controller_child_resource import api as child_resource_ns
# from app.main.controller.controller_resource_policy import api as resource_policy_ns
# from app.main.controller.controller_group_policy import api as group_policy_ns
# from app.main.controller.controller_space import api as space_ns
# from app.main.controller.controller_space_policy import api as space_policy_ns
# from app.main.controller.controller_space_resource import api as space_resource_ns
# from app.main.controller.embeddings.controller_tableau import api as tableau_ns


blueprint_api = Blueprint('api', __name__)



api = Api(blueprint_api,
          title='DB Browser',
          version='X.X.X',
          description='DB Browser API',
          contact='cyes850407@gmail.com'
          )

# jwt_manager._set_error_handler_callbacks(api)

api.namespaces.pop(0)
api.add_namespace(cx_oracle_ns)
# api.add_namespace(config_ns)
# api.add_namespace(permission_ns)
# api.add_namespace(user_ns)
# api.add_namespace(group_ns)
# api.add_namespace(group_policy_ns)
# api.add_namespace(parent_resource_ns)
# api.add_namespace(child_resource_ns)
# api.add_namespace(resource_policy_ns)
# api.add_namespace(space_ns)
# api.add_namespace(space_policy_ns)
# api.add_namespace(space_resource_ns)

# api.add_namespace(tableau_ns)