from flask import Blueprint
from app.main.config import Config
# from flask import current_app
# from app import blueprint

dashboard = Blueprint('dashboard',
                      __name__,
                      static_folder=Config.STATIC_FOLDER,
                      static_url_path='')


@dashboard.after_request
def add_header(response):
    response.headers['X-UA-Compatible'] = 'IE=Edge,chrome=1'
    response.headers['Cache-Control'] = 'public, max-age=0'
    return response

## set the parameter 'strict_slashes' for the vulnerability scan issue
## Plugin Name: Web Server HTTP Header Internal IP Disclosure (10759)
@dashboard.route('/', strict_slashes=False)
def home():
    return dashboard.send_static_file('index.html')
