import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { QueryPageComponent } from "./query-page/query-page.component";

const routes: Routes = [
  { path: '', component: QueryPageComponent },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class OracleSqlRoutingModule { }
