import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { OracleSqlService } from "../../services/oracle-sql/oracle-sql.service";
import { CxOracleRequestBody, CxOracleResponseBody } from "../../models/cx-oralce.model";

@Component({
  selector: 'app-query-page',
  templateUrl: './query-page.component.html',
  styleUrls: ['./query-page.component.scss']
})
export class QueryPageComponent implements OnInit {
  sqlForm: FormGroup;
  displayTable: boolean = false;
  errorMessage: string;

  displayedColumns: string[];
  dataSource: any[];
  constructor(
    private oracleSqlService: OracleSqlService,
    private fb: FormBuilder,
  ) { }

  ngOnInit() {
    this.buildForm();
  }
  buildForm() {
    this.sqlForm = this.fb.group({
      sql: ['', [
      ]
      ],
    });
  }
  getColumnWidth(columnName: string): number {
    return columnName.length * 14
  }

  public submit() {
    console.log(this.sqlForm.value.sql)
    this.errorMessage = '';
    this.displayTable = false;
    const requestBody: CxOracleRequestBody = {
      "sql": this.sqlForm.value.sql
    }
    this.oracleSqlService.queryCxOracle(requestBody).subscribe(
      result => {
        console.log(result);
        this.displayedColumns = result.columns;
        this.dataSource = result.data;
        this.displayTable = true;
      },
      error => {
        console.log(error.error);
        this.errorMessage = error.error.message;
        this.displayTable = false;
      }
    )
  }

}
