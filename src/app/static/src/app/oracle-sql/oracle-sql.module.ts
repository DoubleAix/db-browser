import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ReactiveFormsModule } from '@angular/forms';
import { FlexLayoutModule } from '@angular/flex-layout';

import { MatTableModule } from '@angular/material/table';
import { MatInputModule } from '@angular/material/input';
import { MatButtonModule } from '@angular/material/button';
import { MatIconModule } from '@angular/material';


import { OracleSqlRoutingModule } from './oracle-sql-routing.module';
import { QueryPageComponent } from './query-page/query-page.component';


@NgModule({
  declarations: [QueryPageComponent],
  imports: [
    CommonModule,
    FlexLayoutModule,
    OracleSqlRoutingModule,
    ReactiveFormsModule,
    MatTableModule,
    MatInputModule,
    MatButtonModule,
    MatIconModule,
  ]
})
export class OracleSqlModule { }
