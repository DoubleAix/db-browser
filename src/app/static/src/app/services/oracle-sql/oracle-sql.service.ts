import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { Observable } from 'rxjs';
import { CxOracleRequestBody, CxOracleResponseBody } from "../../models/cx-oralce.model";

@Injectable({
  providedIn: 'root'
})
export class OracleSqlService {
  baseUrl: string = '/api/cx_oracle/';
  constructor(private http: HttpClient) { }

  queryCxOracle(requestBody: CxOracleRequestBody): Observable<CxOracleResponseBody> {
    return this.http.post<CxOracleResponseBody>(this.baseUrl, requestBody);
  }
}
