import { TestBed } from '@angular/core/testing';

import { OracleSqlService } from './oracle-sql.service';

describe('OracleSqlService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: OracleSqlService = TestBed.get(OracleSqlService);
    expect(service).toBeTruthy();
  });
});
