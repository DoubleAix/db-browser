import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';


const routes: Routes = [
  {
    path: 'query',
    loadChildren: () => import('./oracle-sql/oracle-sql.module').then(m => m.OracleSqlModule),
   },
   {
    path: '**',
    redirectTo: 'query'
},
];

@NgModule({
  imports: [RouterModule.forRoot(routes,{
    useHash: true,
   })],
  exports: [RouterModule]
})
export class AppRoutingModule { }
