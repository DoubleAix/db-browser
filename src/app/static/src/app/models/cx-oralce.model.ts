export interface CxOracleResponseBody {
    columns: string[];
    data: object[];
    
}
export interface CxOracleRequestBody {
    sql: string;
}